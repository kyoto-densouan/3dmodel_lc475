# README #

1/3スケールのApple Machintosh LC475風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK Fusion360です。


***

# 実機情報

## メーカ
- 富士通

## 発売時期

- 1993年10月18日発表

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/Macintosh_LC_475)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_lc475/raw/3ddfdc97e5d61d408ca97716fd0489bf3e619a1b/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_lc475/raw/3ddfdc97e5d61d408ca97716fd0489bf3e619a1b/ModelView_2.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_lc475/raw/3ddfdc97e5d61d408ca97716fd0489bf3e619a1b/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_lc475/raw/3ddfdc97e5d61d408ca97716fd0489bf3e619a1b/ExampleImage.jpg)
